# Final Project in the Course Advanced Web Development 2

## This is a Recipe application, where users can create a account to save and remove recipes to/from the users favorites.

### The application is build mobile first, users get the best user experience by using the application in iphone 6 view.

- [LINK TO DEPLOYED WEBSITE](https://slutproject-aw2-fd34e.firebaseapp.com)

## Getting Started

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes. See deployment for notes on
how to deploy the project on a live system.

### Installing

A step by step series of examples that tell you have to get a development env
running

Install all projects dependencys

```
yarn or npm install
```

Run application

```
yarn start or npm run start
```

## Built With

- [React](https://reactjs.org/) - a JavaScript library for building the user
  interfaces.
- [Redux](https://redux.js.org/) - state container for the application.
- [Typescript](https://www.typescriptlang.org/) - typed superset of Javascript.
  that complies to plain Javascript
- [React-router](https://reacttraining.com/react-router/core/guides/philosophy) - Declarative routing for React.
- [Redux-observable](https://redux-observable.js.org/) - RxJS 6-based middleware for Redux. Compose and cancel async actions to create side effects and more.
- [Firebase](https://firebase.google.com/) - Hosting, realtime database and authentication.
- [Styled Components](https://www.styled-components.com/) - Library to write modular css with javascript.

## Authors

- **Samuel Gustafsson** - _Initial work_ -

## License

This project is licensed under the MIT License - see the
[LICENSE.md](LICENSE.md) file for details

Link to materialdesign colors: https://material.io/tools/color/#!/?view.left=0&view.right=0&primary.color=344955&secondary.color=ffbb00

The application is built with react-redux and retrieves news from a REST-API built with node.js

## Getting Started

### Installing

A step by step serie to start project

```
yarn
```

Run this command from the root folder.:

```
yarn start
```

- [View Project here](https://slutproject-aw2-fd34e.firebaseapp.com).

## Built With

- [Firebase](https://firebase.google.com/products/) - Realtime database, hosting and authentication.
- [React](https://reactjs.org/) - Javascript library for building user interfaces.
- [Redux](http://redux.js.org/docs/basics/UsageWithReact.html) - Redux is a predictable state container for JavaScript apps.
- [React router](hhttps://reacttraining.com/react-router/web/guides/philosophy) - routing.
- [Typescript](https://www.typescriptlang.org/) - ´Typescript is a typed superset of Javascript that compiles to plain Javascript.
- [Rxjs](http://mongoosejs.com/) - Mongoose provides a straight-forward, schema-based solution to model your application data for MongoDB.
- [Redux-Observable](https://redux-observable.js.org/) - RxJS 6-based middleware for Redux. Compose and cancel async actions to create side effects and more.

## Authors

- **Samuel Gustafsson** - [Linkedin](https://www.linkedin.com/in/samuel-gustafsson)
