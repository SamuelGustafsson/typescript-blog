import * as React from "react";
import { Login } from "./authentication";
import { Header } from "./header";
import { Route, Switch } from "react-router";
import { DetailPage } from "./recepts/detail";
import { RecipeList } from "./recepts/list";
import { Favorites } from "src/recepts/favorite";
import { ProtectedRoute, firebase } from "src/shared";
import { store } from "src";
import { history } from "src/history";
import { Actions } from "src/actions";
require("./recepts/load/listenForRecipes");

class AppComponent extends React.Component<{}, {}> {
  componentDidMount() {
    firebase.auth().onAuthStateChanged((user: firebase.User) => {
      !user ? history.push("/login") : store.dispatch(Actions.loginFulfilled(user));
      firebase
        .database()
        .ref(`/users/${user.uid}/favorites`)
        .on("value", (snapshot: firebase.database.DataSnapshot) => {
          store.dispatch(Actions.loadFavoriteRecipesSuccessfully(snapshot.val()));
        });
      history.push("/");
    });
  }

  render() {
    return (
      <>
        <Route path="/" component={Header} />
        <Switch>
          <Route path="/recipe" render={() => <DetailPage />} />
          <Route path="/login" render={() => <Login />} />
          <ProtectedRoute path="/favorites" render={() => <Favorites />} />
          <Route path="/" component={RecipeList} />
        </Switch>
      </>
    );
  }
}

export { AppComponent as App };
