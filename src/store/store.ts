import { createStore, compose, applyMiddleware } from "redux";
import { createEpicMiddleware } from "redux-observable";
import { LoggerMiddleware } from "./logger.middleware";
import { connectRouter, routerMiddleware } from "connected-react-router";

import { rootReducer } from "../reducers";
import { rootEpic } from "../epics";

import { history } from "../history";

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const epicMiddleware = createEpicMiddleware();

type AppState = ReturnType<typeof rootReducer>;

const ConfigureStore = () => {
  const store = createStore(
    connectRouter(history)(rootReducer), // new root reducer with router state
    composeEnhancers(
      applyMiddleware(
        epicMiddleware,
        LoggerMiddleware,
        routerMiddleware(history) // for dispatching history actions
      )
    )
  );
  epicMiddleware.run(rootEpic);
  return store;
};

export { AppState, ConfigureStore };
