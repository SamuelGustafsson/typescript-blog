// tslint:disable:no-any
export const LoggerMiddleware = (_store: any) => (next: any) => (
  action: any
) => {
  console.log(
    "%cAction %c" + action.type,
    "padding: 2px 4px; background-color: #f0f0f0; color: #7A8B98",
    "padding: 2px 4px; background-color: #f0f0f0; color: #6B8F1C",
    action
  );
  return next(action);
};
