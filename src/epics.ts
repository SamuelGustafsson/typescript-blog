import { combineEpics } from "redux-observable";
import { importToArray } from "./shared";

import * as authEpics from "./authentication/epics";

const epics = [...importToArray(authEpics)];

const rootEpic = combineEpics<any>(...epics);

export { rootEpic };
