import { ActionsObservable } from "redux-observable";
import { ActionTypes, Actions } from "src/actions";
import { auth } from "src/shared/firebase/firebase";
import { tap, ignoreElements } from "rxjs/operators";
import { history } from "src/history";

const LogoutEpic = (action$: ActionsObservable<any>) =>
  action$.ofType(ActionTypes.LOGOUT).pipe(
    tap(() => {
      auth.signOut().then(() => Actions.logout());
      history.push("/login");
    }),
    ignoreElements()
  );

export { LogoutEpic };
