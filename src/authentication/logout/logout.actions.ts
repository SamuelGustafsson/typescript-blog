import { createAction, ActionsUnion } from "../../shared";

export const ActionTypes = {
  LOGOUT: "LOGOUT" as "LOGOUT"
};

export const Actions = {
  logout: () => createAction(ActionTypes.LOGOUT)
};

export type Actions = ActionsUnion<typeof Actions>;
