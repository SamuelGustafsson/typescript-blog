import { ActionsUnion, createAction, firebase } from "src/shared";

export const ActionTypes = {
  SIGN_UP_PENDING: "SIGN_UP_PENDING" as "SIGN_UP_PENDING",
  SIGN_UP_FULFILLED: "SIGN_UP_FULFILLED" as "SIGN_UP_FULFILLED",
  SIGN_UP_REJECTED: "SIGN_UP_REJECTED" as "SIGN_UP_REJECTED"
};

export const Actions = {
  signUp: (email: string, password: string) =>
    createAction(ActionTypes.SIGN_UP_PENDING, {
      email,
      password
    }),
  signUpFulfilled: (user: firebase.User) =>
    createAction(ActionTypes.SIGN_UP_FULFILLED, { user }),
  signUpRejected: (errorMessage: string) =>
    createAction(ActionTypes.SIGN_UP_REJECTED, { errorMessage })
};

export type Actions = ActionsUnion<typeof Actions>;
