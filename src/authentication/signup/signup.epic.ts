import { ActionsObservable } from "redux-observable";
import { ActionTypes, Actions } from "src/actions";
import { auth } from "src/shared/firebase/firebase";
import { from, of } from "rxjs";
import { mergeMap, map, catchError } from "rxjs/operators";
import { User as FirebaseUser } from "firebase";

const signUpEpic = (action$: ActionsObservable<any>) =>
  action$.ofType(ActionTypes.SIGN_UP_PENDING).pipe(
    mergeMap(action =>
      from<FirebaseUser>(
        auth.createUserWithEmailAndPassword(action.payload.email, action.payload.password)
      ).pipe(
        map(user => {
          return Actions.signUpFulfilled(user);
        }),
        catchError(_error => {
          return of(Actions.signUpRejected(_error.message));
        })
      )
    )
  );

export { signUpEpic };
