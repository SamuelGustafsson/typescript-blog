import { ActionsObservable } from "redux-observable";
import { ActionTypes, Actions } from "src/actions";
import { auth } from "src/shared";
import { from, of } from "rxjs";
import { mergeMap, map, catchError } from "rxjs/operators";
import { User as FirebaseUser } from "firebase";
import { history } from "src/history";

const LoginEpic = (action$: ActionsObservable<any>) =>
  action$.ofType(ActionTypes.LOGIN_PENDING).pipe(
    mergeMap(action =>
      from<FirebaseUser>(
        auth.signInWithEmailAndPassword(action.payload.email, action.payload.password)
      ).pipe(
        map((user: any) => {
          history.push("/");
          return Actions.loginFulfilled(user);
        }),
        catchError((_error: any) => {
          return of(Actions.loginRejected(_error.message));
        })
      )
    )
  );

export { LoginEpic };
