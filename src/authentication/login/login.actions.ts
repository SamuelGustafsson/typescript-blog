import { createAction, ActionsUnion, firebase } from "../../shared";

export const ActionTypes = {
  LOGIN_PENDING: "LOGIN_PENDING" as "LOGIN_PENDING",
  LOGIN_FULFILLED: "LOGIN_FULFILLED" as "LOGIN_FULFILLED",
  LOGIN_REJECTED: "LOGIN_REJECTED" as "LOGIN_REJECTED"
};

export const Actions = {
  login: (email: string, password: string) =>
    createAction(ActionTypes.LOGIN_PENDING, {
      email,
      password
    }),
  loginFulfilled: (user: firebase.User) =>
    createAction(ActionTypes.LOGIN_FULFILLED, { user }),
  loginRejected: (errorMessage: string) =>
    createAction(ActionTypes.LOGIN_REJECTED, { errorMessage })
};

export type Actions = ActionsUnion<typeof Actions>;
