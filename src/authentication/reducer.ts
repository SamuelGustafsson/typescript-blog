import { Actions, ActionTypes } from "../actions";
import { firebase } from "src/shared";

interface State {
  authenticating: boolean;
  authenticated: boolean;
  user?: firebase.User;
  errorMessage?: string;
}

const initialState: State = {
  authenticating: false,
  authenticated: false
};

const reducer = (state = initialState, action: Actions): State => {
  const {
    LOGIN_PENDING,
    LOGIN_FULFILLED,
    LOGIN_REJECTED,
    LOGOUT,
    SIGN_UP_PENDING,
    SIGN_UP_FULFILLED,
    SIGN_UP_REJECTED
  } = ActionTypes;

  switch (action.type) {
    case LOGIN_PENDING:
      return { ...state, authenticating: true };
    case LOGIN_FULFILLED:
      return {
        ...state,
        authenticating: false,
        authenticated: true,
        user: action.payload.user
      };
    case LOGIN_REJECTED:
      return {
        ...state,
        authenticating: false,
        authenticated: false,
        errorMessage: action.payload.errorMessage
      };

    case LOGOUT:
      return { ...state, authenticated: false, errorMessage: "", user: undefined };

    case SIGN_UP_PENDING:
      return { ...state, authenticating: true };

    case SIGN_UP_FULFILLED:
      return {
        ...state,
        authenticating: false,
        authenticated: true,
        errorMessage: "",
        user: action.payload.user
      };

    case SIGN_UP_REJECTED:
      return {
        ...state,
        authenticating: false,
        authenticated: false,
        errorMessage: action.payload.errorMessage
      };

    default:
      return state;
  }
};

export { reducer as authReducer, State as AuthReducerState };
