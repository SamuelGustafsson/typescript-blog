import {
  styled,
  Textbox,
  VerticalDivider,
  Button,
  Title,
  ErrorMessage
} from "src/shared";
import * as React from "react";
import { Actions } from "../actions";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { AppState } from "src/store";
import { Spinner } from "../shared/spinner";

type Props = InjectedDispatchProps & InjectedStateProps;

type ComponentState = {
  email: string;
  password: string;
  className?: string;
};

type InjectedDispatchProps = {
  login: (email: string, password: string) => void;
  signUp: (email: string, password: string) => void;
};

type InjectedStateProps = {
  authenticated: boolean;
  loading: boolean;
  error?: string;
};

class LoginComponent extends React.Component<Props, ComponentState> {
  state = {
    email: "",
    password: ""
  };

  login = () => {
    const { email, password } = this.state;
    this.props.login(email, password);
  };
  signUp = () => {
    const { email, password } = this.state;
    this.props.signUp(email, password);
  };

  render() {
    return (
      <Container>
        {this.props.loading ? (
          <Spinner />
        ) : (
          <>
            <VerticalDivider large={true} />
            <h1>Sign in</h1>
            <Input
              value={this.state.email}
              onChange={(value: string) => this.setState({ email: value })}
              type="email"
              placeholder="Email"
            />
            <Input
              value={this.state.password}
              onChange={(value: string) => this.setState({ password: value })}
              type="password"
              placeholder="Password"
            />

            {this.props.error && (
              <>
                <ErrorMessage>{this.props.error}</ErrorMessage>{" "}
                <VerticalDivider height={20} />
              </>
            )}
            <Button onClick={this.login}>
              <Title value="Logga in" color="white" />
            </Button>
            <VerticalDivider height={20} />
            <Button onClick={this.signUp}>
              <Title
                value="registrera nytt konto"
                caseType="upperCaseFirst"
                color="white"
              />
            </Button>
          </>
        )}
      </Container>
    );
  }
}

const Input = styled(Textbox)`
  padding-left: 10px;
`;

const Container = styled.section`
  display: flex;
  align-content: center;
  flex-direction: column;
  align-items: center;

  & > * {
    min-width: 250px !important;
    max-width: 250px !important;
  }
`;

const mapDispatchToProps = (dispatch: Dispatch<Actions>): InjectedDispatchProps => ({
  login: (email: string, password: string) => dispatch(Actions.login(email, password)),
  signUp: (email: string, password: string) => dispatch(Actions.signUp(email, password))
});

const mapStateToProps = (state: AppState): InjectedStateProps => ({
  authenticated: state.authentication.authenticated,
  loading: state.authentication.authenticating,
  error: state.authentication.errorMessage && state.authentication.errorMessage
});

const Login = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginComponent);

export { Login };
