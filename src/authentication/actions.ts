import * as login from "./login/login.actions";
import * as logout from "./logout/logout.actions";
import * as signUp from "./signup/signup.actions";
import { ActionsUnion } from "../shared";

export const ActionTypes = {
  ...login.ActionTypes,
  ...logout.ActionTypes,
  ...signUp.ActionTypes
};

export const Actions = {
  ...login.Actions,
  ...logout.Actions,
  ...signUp.Actions
};

export type Actions = ActionsUnion<typeof Actions>;
