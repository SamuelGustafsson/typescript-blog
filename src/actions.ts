import { ActionsUnion } from "./shared";

import * as auth from "./authentication/actions";
import * as recipe from "./recepts/actions";

export const ActionTypes = {
  ...auth.ActionTypes,
  ...recipe.ActionTypes
};

export const Actions = {
  ...auth.Actions,
  ...recipe.Actions
};

export type Actions = ActionsUnion<typeof Actions>;
