import * as React from "react";
import { Route, RouteProps, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { AppState } from "src/store";

type Props = RouteProps & InjectedStateProps;

type InjectedStateProps = {
  authenticated: boolean;
};

class ProtectedRouteComponent extends Route<Props> {
  render() {
    let redirectPath: string = "";
    !this.props.authenticated && (redirectPath = "/login");

    if (redirectPath) {
      const renderComponent = () => <Redirect to={{ pathname: redirectPath }} />;
      return <Route {...this.props} component={renderComponent} render={undefined} />;
    } else {
      return <Route {...this.props} />;
    }
  }
}

const mapStateToProps = (state: AppState): InjectedStateProps => ({
  authenticated: state.authentication.authenticated
});

const ProtectedRoute = connect(mapStateToProps)(ProtectedRouteComponent);

export { ProtectedRoute };
