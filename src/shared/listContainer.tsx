import { styled } from ".";

// const ListContainerComponent = styled.div`
//   display: flex;
//   align-items: stretch;
//   flex-direction: row;
//   flex-wrap: wrap;
//   justify-content: center;
// `;
const ListContainerComponent = styled.div`
  display: grid;
  max-width: 1000px;
  margin: 200px auto;
  grid-gap: 10px 50px;
  grid-template-columns: 3fr 12fr 5fr;
`;

export { ListContainerComponent as ListContainer };
