import { styled, withProps } from "..";

const ButtonContainerComponent = withProps<{
  disabled?: boolean;
  minWidth?: number;
}>(styled.div)`
    display: flex;
    align-items: center;
    justify-content: center;
    text-transform: uppercase;
    font-size: 16px;
    padding: 14px 28px;
    padding-top: 16px;
    user-select: none;
    cursor: pointer;
    min-width: ${props => (props.minWidth && props.minWidth + "px") || "unset"};
    background-color: ${props =>
      !!props.disabled
        ? props.theme.colors.primary.light
        : props.theme.colors.primary.color};
    color: ${props => props.theme.colors.primary.text};

    ${props =>
      !props.disabled &&
      `&: hover {
            background-color: ${props.theme.colors.primary.light}
        }
        &:active {
            background-color: ${props.theme.colors.primary.dark};
        }
        `}

`;

export { ButtonContainerComponent as ButtonContainer };
