import * as React from "react";
import { ButtonContainer } from "..";

type Props = StateProps & DispatchProps;
type StateProps = {
  disabled?: boolean;
  className?: string;
  hide?: boolean;
  minWidth?: number;
};
type DispatchProps = {
  onClick?: () => void;
  children?: string | JSX.Element;
};
const ButtonComponent = (props: Props) => {
  const click = () => !props.disabled && props.onClick && props.onClick();

  return !props.hide ? (
    <ButtonContainer
      className={props.className}
      onClick={click}
      disabled={props.disabled}
      minWidth={props.minWidth}
    >
      {props.children}
    </ButtonContainer>
  ) : null;
};

export { ButtonComponent as Button };
