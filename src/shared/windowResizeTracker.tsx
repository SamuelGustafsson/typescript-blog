import * as React from "react";
import { Subscription, fromEvent } from "rxjs";
import { debounceTime } from "rxjs/operators";

type Props = StateProps;
interface StateProps {
  /**
   * Renders children using a function and injects current width and height
   *
   * @memberof StateProps
   */
  children?: (width: number, height: number) => JSX.Element;
}
interface ComponentState {
  subscriptions: Subscription[];
  width: number;
  height: number;
}

/**
 * Tracks window size and passes width and height to children.
 *
 * @class WindowResizeTrackerComponent
 * @extends {React.Component<Props, ComponentState>}
 */
class WindowResizeTrackerComponent extends React.Component<Props, ComponentState> {
  constructor(props: Props) {
    super(props);
    this.state = {
      subscriptions: [],
      width: window.innerWidth,
      height: window.innerHeight
    };
  }

  componentDidMount() {
    this.setState({
      subscriptions: [
        ...this.state.subscriptions,
        fromEvent<Event>(window, "resize")
          // Observable.fromEvent<Event>(window, "resize")
          .pipe(debounceTime(200))
          .subscribe(() => {
            this.setState({
              width: window.innerWidth,
              height: window.innerHeight
            });
          })
      ]
    });
  }

  componentWillUnmount() {
    this.state.subscriptions.forEach(subscription => subscription.unsubscribe);
  }

  render() {
    const { width, height } = this.state;
    return this.props.children
      ? (this.props.children as (width: number, height: number) => JSX.Element)(
          width,
          height
        )
      : null;
  }
}

export { WindowResizeTrackerComponent as WindowResizeTracker };
