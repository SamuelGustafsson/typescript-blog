import * as React from "react";
import * as changeCase from "change-case";
import { styled } from "src/shared";
import { CaseType } from "./case-types";

type Props = StateProps & StylingProps;
type StateProps = {
  value: string;
};

type StylingProps = {
  caseType?: CaseType;
  color?: string;
  fontSize?: number;
  fontWeight?: string;
};

const TitleComponent = (props: Props) => {
  const { value, color, fontSize, fontWeight } = props;

  const result = props.caseType ? changeCase[props.caseType](value.toLowerCase()) : value;

  return (
    <TitleContainer color={color} fontSize={fontSize} fontWeight={fontWeight}>
      {result}
    </TitleContainer>
  );
};

const TitleContainer = styled.div<StylingProps>`
  color: ${props => props.color && props.color};
  font-size: ${props => props.fontSize && props.fontSize}px;
  font-weight: ${props => props.fontWeight};
`;

export { TitleComponent as Title };
