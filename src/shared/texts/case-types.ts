type CaseType =
  | "camelCase"
  | "constantCase"
  | "dotCase"
  | "headerCase"
  | "lowerCase"
  | "lowerCaseFirst"
  | "noCase"
  | "paramCase"
  | "snakeCase"
  | "swapCase"
  | "titleCase"
  | "upperCase"
  | "upperCaseFirst";

export { CaseType };
