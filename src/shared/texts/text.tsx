import * as React from "react";
import * as changeCase from "change-case";
import { styled } from "..";
import { CaseType } from "./case-types";

type Props = StateProps & StylingProps;
type StateProps = {
  value: string;
};

type StylingProps = {
  caseType?: CaseType;
  color?: string;
  fontSize?: number;
  maxWidth?: number;
  minWidth?: number;
  fontWeight?: string;
};

const TextComponent = (props: Props) => {
  const { value, color, fontSize, maxWidth, minWidth, fontWeight } = props;

  const result =
    (props.caseType && changeCase[props.caseType](value.toLowerCase())) || value;

  return (
    <TextContainer
      color={color}
      fontSize={fontSize}
      maxWidth={maxWidth}
      minWidth={minWidth}
      fontWeight={fontWeight}
    >
      {result}
    </TextContainer>
  );
};

const TextContainer = styled.span<StylingProps>`
  color: ${props => props.color && props.color};
  min-width: ${props => props.minWidth || 40}px;
  max-width: ${props => (props.maxWidth && props.maxWidth + "px") || "unset"};
  font-size: ${props => props.fontSize && props.fontSize}px;
  font-weight: ${props => props.fontWeight};
  font-family: ${props => props.theme.fonts.primary};
`;

export { TextComponent as Text };
