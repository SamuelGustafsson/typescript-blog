type ImportObject<PropType> = {
  [key: string]: PropType;
};

/**
 * Takes an import, e.g. allItems in 'import * as allItems from "./a-lot-of-exported-items"'
 * and turns it into an array with types intact
 */
const importToArray = <PropType>(importObject: ImportObject<PropType>): PropType[] =>
  Object.keys(importObject).map(key => importObject[key]);

export { importToArray };
