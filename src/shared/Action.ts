import { ActionCreatorsMapObject } from "redux";

// Our ActionsUnion will help us automatically combine all our actions
// with their action types and payload types.
export type ActionsUnion<A extends ActionCreatorsMapObject> = ReturnType<
  A[keyof A]
>;

// Here we define two types of action interfaces
// One with only type:
export interface Action<ActionType extends string> {
  type: ActionType;
}
// ..and one with type and a generic payload:
export interface ActionWithPayload<ActionType extends string, Payload>
  extends Action<ActionType> {
  payload: Payload;
}

// And lastly we define action creator functions that will help
// us create above easily with all types intact
export function createAction<ActionType extends string>(
  type: ActionType
): Action<ActionType>;
export function createAction<ActionType extends string, Payload>(
  type: ActionType,
  payload: Payload
): ActionWithPayload<ActionType, Payload>;
export function createAction<ActionType extends string, Payload>(
  type: ActionType,
  payload?: Payload
) {
  return payload === undefined ? { type } : { type, payload };
}
