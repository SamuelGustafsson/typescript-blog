import { withProps, styled, TextboxStateProps } from "..";

const TextInputComponent = withProps<TextboxStateProps>(styled.input).attrs({
  placeholder: (props: TextboxStateProps) => props.placeholder
})`
    background-color: ${props => (props.error ? "#ffb6c1" : "white")};
    color: ${props => (props.error ? "red" : "unset")}
    border: ${props => {
      return !props.hasOwnProperty("originalValue") ||
        props.originalValue === props.value ||
        (!props.originalValue && !props.value)
        ? "1px solid #ddd"
        : "1px solid orange";
    }};
    width: ${props =>
      !!props.width
        ? typeof props.width === "string"
          ? props.width
          : props.width + "px"
        : "unset"};
    padding: 10px;

    &::placeholder {
        color: #aaa;
    }
    
    @media print {
        border: none;
        background-color: transparant;
    }
`;

export { TextInputComponent as TextInput };
