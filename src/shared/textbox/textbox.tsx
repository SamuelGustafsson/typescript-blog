import * as React from "react";
import { ErrorMessage, TextArea, TextInput } from "..";

type Props = StateProps & DispatchProps;
interface StateProps {
  value?: string | number;
  placeholder?: string;
  multiline?: boolean | number;
  error?: string;
  originalValue?: string | number;
  ignoreEmptyStrings?: boolean;
  className?: string;
  width?: string | number;
  password?: boolean;
  disabled?: boolean;
  enabledOnReadAccess?: boolean;
  type?: string;
  step?: number;
  min?: string;
  max?: string;
}
interface DispatchProps {
  onChange?: (text: string) => void;
  onEnterKeyPress?: () => void;
  onBlur?: (text: string) => void;
  onFocus?: () => void;
}

type ComponentState = {
  value: string;
};

class TextboxComponent extends React.PureComponent<Props, ComponentState> {
  state = { value: `${this.props.value}` || "" };

  componentDidMount() {
    this.setState({ value: `${this.props.value}` });
  }

  onKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter" && this.props.onEnterKeyPress) this.props.onEnterKeyPress();
  };

  change = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ value: event.target.value });

    if (!this.props.onChange) {
      return;
    }
    const value = event.target.value;
    this.props.onChange(value);
  };

  blur = () => {
    this.props.onBlur && this.props.onBlur(this.state.value);
  };

  render() {
    const Text = !this.props.multiline ? TextInput : TextArea;

    const conditionalProps: {
      originalValue?: string | number;
      value?: string | number;
    } = this.props.hasOwnProperty("originalValue")
      ? { originalValue: this.props.originalValue }
      : {};

    return (
      <>
        <Text
          className={this.props.className}
          width={this.props.width}
          value={this.state.value}
          onChange={this.change}
          onKeyPress={this.onKeyPress}
          onBlur={this.blur}
          onFocus={this.props.onFocus}
          disabled={!!this.props.disabled}
          type={this.props.type}
          step={this.props.step}
          min={this.props.min}
          max={this.props.max}
          {...conditionalProps}
          {...(this.props.password && { type: "password" }) || {}}
        />
        {!!this.props.error && <ErrorMessage>{this.props.error}</ErrorMessage>}
      </>
    );
  }
}

export { TextboxComponent as Textbox, StateProps as TextboxStateProps };
