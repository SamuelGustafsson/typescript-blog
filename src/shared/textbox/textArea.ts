import { withProps, styled, TextboxStateProps } from "..";

const TextAreaComponent = withProps<TextboxStateProps>(styled.textarea).attrs({
  placeholder: (props: TextboxStateProps) => props.placeholder,
  rows: (props: TextboxStateProps) =>
    (typeof props.multiline === "number" && props.multiline > 0 && props.multiline) || 5
})`
    background-color: ${props => (props.error ? "ffb6c1" : "white")};
    color: ${props => (props.error ? "red" : "unset")}
    border: ${props =>
      !props.hasOwnProperty("originalValue") ||
      props.originalValue === props.value ||
      (!props.originalValue && !props.value)
        ? "1px solid #ddd"
        : "1px solid orange"};
    width: ${props =>
      !!props.width
        ? typeof props.width === "string"
          ? props.width
          : props.width + "px"
        : "380px"};
    padding: 10px;

    &::placeholder {
        color:  #aaa;
    }

    @media print {
        border: none;
        background-color: transparant;
    }
`;

export { TextAreaComponent as TextArea };
