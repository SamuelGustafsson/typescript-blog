import { styled } from "..";

const ErrorMessageComponent = styled.span`
  margin-top: 2px;
  color: red;
`;

export { ErrorMessageComponent as ErrorMessage };
