import { styled } from ".";
import { withProps } from "./withProps";

type VerticalProps = SizeProps & {
  height?: number;
};
type HorizontalProps = SizeProps & {
  width?: number;
};

type SizeProps = {
  xs?: boolean;
  small?: boolean;
  medium?: boolean;
  large?: boolean;
  xl?: boolean;
};

const CalculateSize = (sizes: SizeProps, fallback: number): number =>
  (sizes.xs && 5) ||
  (sizes.small && 10) ||
  (sizes.medium && 20) ||
  (sizes.large && 30) ||
  (sizes.xl && 40) ||
  fallback;

const HorizontalDividerComponent = withProps<HorizontalProps>(styled.div)`
    height: 100%;
    max-height: 100%;
    min-height: 100%;
    width: ${props => props.width || CalculateSize(props, 10)}px;
    max-width: ${props => props.width || CalculateSize(props, 10)}px;
    min-width: ${props => props.width || CalculateSize(props, 10)}px;
`;
const VerticalDividerComponent = withProps<VerticalProps>(styled.div)`
    width: 100%;
    max-width: 100%;
    min-width: 100%;
    height: ${props => props.height || CalculateSize(props, 10)}px;
    max-height: ${props => props.height || CalculateSize(props, 10)}px;
    min-height: ${props => props.height || CalculateSize(props, 10)}px;
`;

export {
  VerticalDividerComponent as VerticalDivider,
  HorizontalDividerComponent as HorizontalDivider
};
