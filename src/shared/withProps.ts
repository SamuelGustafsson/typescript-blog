// tslint:disable:no-any
import * as React from "react";
import { ThemedStyledFunction } from "styled-components";
import { Theme } from "./styles/styled-components/theme";
/**
 * Helper styled-components wrapper to tell typescript that a styled component
 * Requires given props
 */
const withProps = <
  PropsType,
  ThemeType = Theme,
  ElementType extends HTMLElement = HTMLElement
>(
  styledFunction: ThemedStyledFunction<any, any>
): ThemedStyledFunction<PropsType & React.HTMLProps<ElementType>, ThemeType> => {
  return styledFunction;
};

export { withProps };
