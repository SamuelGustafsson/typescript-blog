import * as firebase from "firebase";
import { config } from "./config/keys";

firebase.initializeApp(config);

const database = firebase.database();
const auth = firebase.auth();

const recipesRef = database.ref("/recipes");

export { firebase, database, auth, recipesRef };
