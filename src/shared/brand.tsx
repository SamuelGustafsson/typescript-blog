import { styled } from ".";

const Brand = styled.h1`
  font-family: ${props => props.theme.fonts.brand};
  font-size: 24px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.46;
  letter-spacing: normal;
  text-align: left;
  color: rgba(255, 255, 255, 0.87);
  text-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
  margin: 0 auto;
`;

export { Brand };
