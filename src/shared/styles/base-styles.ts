import { injectGlobal } from "./styled-components";
import styledNormalize from "styled-normalize";

export const baseStyles = () => injectGlobal`

${styledNormalize}

    @font-face {
        font-family: Shrikhand;
        src: url(${require("./fonts/Shrikhand/Shrikhand-Regular.ttf")});
        font-weight: normal;
    }

        @font-face {
        font-family: Roboto;
        src: url(${require("./fonts/Roboto/Roboto-Regular.ttf")});
        font-weight: normal;
    }


/*
  WAT IS THIS?!
  We inherit box-sizing: border-box; from our <html> selector
  Apparently this is a bit better than applying box-sizing: border-box; directly to the * selector
*/
  *,
  *:before,
  *:after {
    box-sizing: inherit;
  }

  html {
    /* border-box box model allows us to add padding and border to our elements without increasing their size */
    box-sizing: border-box;
    font-size: 16px;
    height: 100%;

    font-family: Helvetica, Arial, sans-serif;
  }

  p {
    margin: 0px;
  }

  /* html, body, #root {
    display: grid;
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
    border: 0; 
} */
html, body, #root {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
            border: 0;
        }
`;
