import * as styledComponents from "styled-components";
import { ThemedStyledComponentsModule } from "styled-components";

import { Theme } from "./theme";

// Re-export styled components functions
// with our theme interface set as default theme model
// this will help us get typescript intellisense when
// calling on 'props.theme' in a styled component

const {
  default: styled,
  css,
  injectGlobal,
  keyframes,
  ThemeProvider
} = styledComponents as ThemedStyledComponentsModule<Theme>;

export { styled, css, injectGlobal, keyframes, ThemeProvider };
export * from "./theme";
