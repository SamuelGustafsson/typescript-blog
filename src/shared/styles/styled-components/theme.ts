interface Fonts {
  primary: string;
  brand: string;
}

interface Theme {
  fonts: Fonts;
  colors: {
    primary: {
      color: string;
      light: string;
      dark: string;
      text: string;
    };
    secondary: {
      color: string;
      light: string;
      dark: string;
      text: string;
    };
    background: {
      grey: string;
    };
  };
  mobile: {
    padding: number;
  };
}

const theme: Theme = {
  fonts: {
    primary: "Roboto",
    brand: "Shrikhand"
  },
  colors: {
    primary: {
      color: "#344955",
      light: "#5F7481",
      dark: "#344955",
      text: "#ffffff"
    },
    secondary: {
      color: "#FFBB00",
      light: "#FFDC4A",
      dark: "#707070",
      text: "#ffffff"
    },
    background: {
      grey: "#EDEDEE"
    }
  },
  mobile: {
    padding: 16
  }
};

export { theme, Theme };
