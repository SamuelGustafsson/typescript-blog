import * as React from "react";
import { styled } from "src/shared";
import { withProps } from "src/shared";
import { keyframes } from "styled-components";

type Props = {
  mini?: boolean;
};

const SpinnerComponent = (props: Partial<Props>) => {
  return (
    <SpinnerContainer {...props}>
      <div className="rect1" />
      <div className="rect2" />
      <div className="rect3" />
      <div className="rect4" />
      <div className="rect5" />
    </SpinnerContainer>
  );
};

const stretch = keyframes`
    0%,
    40%,
    100% {
        transform: scaleY(0.4);
    }
    20% {
        transform: scaleY(1.0);
    }
`;

const SpinnerContainer = withProps<Props>(styled.div)`
    width: ${props => (props.mini ? 100 / 5 : 100)}px;
    height: ${props => (props.mini ? 80 / 5 : 80)}px;
    text-align: center;
    font-size: 10px;

    & > div {
        background-color: ${props => props.theme.colors.secondary.color};
        height: 100%;
        width: ${props => (props.mini ? 10 / 5 : 10)}px;
        display: inline-block;
        animation: ${stretch} 1.2s infinite ease-in-out;
        margin-left: ${props => (props.mini ? 1 : 2)}px;
    }

    & .rect1 {
        animation-delay: -2s;
    }

    & .rect2 {
        animation-delay: -1.9s;
    }

    & .rect3 {
        animation-delay: -1.8s;
    }

    & .rect4 {
        animation-delay: -1.7s;
    }

    & .rect5 {
        animation-delay: -1.6s;
    }
`;

export { SpinnerComponent as Spinner };
