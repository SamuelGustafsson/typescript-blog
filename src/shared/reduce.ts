/*
 * Generic helper function to create mini-reducers when splitting
 * a large reducer up in smallar chunks, chunks that still handles the same
 * reducer state.
 */
const reduce = <State extends object, Action extends object>(
    reducer: (state: State, action: Action) => State,
) => reducer;

export { reduce };
