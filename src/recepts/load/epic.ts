// // import { ActionsObservable, ofType } from "redux-observable";
// import { Actions } from "src/actions";
// import { store } from "src/index";

// // import { switchMap,  catchError } from "rxjs/operators";
// // import {  of } from "rxjs";
// import { recipesRef } from "../../shared";

// const loadSuccessfull = (recipes: { [key: string]: any }) => {
//   const action = Actions.loadRecipesSuccessfully(recipes);
//   store.dispatch(action);
// };

// // tslint:disable-next-line:no-any
// const loadRecipesEpic = (action$: ActionsObservable<any>) =>
//   action$.pipe(
//     ofType(ActionTypes.FETCH_RECEPTS_PENDING),
//     switchMap(() =>
//       recipesRef.once("value", snapshot =>
//         Actions.loadRecipesSuccessfully(snapshot.val())
//       )
//     ),
//     catchError(error =>
//       of(Actions.loadRecipesRejected("Det gick åt helvete med hämtningen av recept"))
//     )

//   );

// export { loadRecipesEpic };

// import { ActionsObservable } from "redux-observable";
// import * as Rx from "rxjs";
// import { Actions, ActionTypes } from "@src/actions";

// // tslint:disable-next-line:no-any
// const initEpic = (action$: ActionsObservable<any>) =>
//   action$
//     .ofType(ActionTypes.INIT_PENDING)
//     .mergeMap(() => {
//       let countryCode = navigator.language.substring(3);
//       if (countryCode.toUpperCase() !== "SE" || countryCode.toUpperCase() !== "DK")
//         countryCode = "SE";

//       return [
//         Actions.loadRetailers(countryCode),
//         Actions.loadProducts(countryCode),
//         Actions.initFulfilled()
//       ];
//     })
//     .catch(_error =>
//       Rx.Observable.of(Actions.initRejected("Application initialization failed"))
//     );

// export { initEpic };
