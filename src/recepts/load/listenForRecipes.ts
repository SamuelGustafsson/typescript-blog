import { Actions } from "src/actions";
import { store } from "src/";

import { recipesRef } from "src/shared";
import { Recipe } from "../recipe";

const loadSuccessfull = (recipes: Array<Recipe>) => {
  const action = Actions.loadRecipesSuccessfully(recipes);
  store.dispatch(action);
};

const listenForRecipies = () => {
  recipesRef.on("value", snapshot => {
    const recipes = snapshot && snapshot.val();

    const arr = Object.keys(recipes).map(key => {
      recipes[key]._id = key;
      return recipes[key];
    });

    snapshot && loadSuccessfull(arr);
  });
};
const RRRRR = listenForRecipies();
export { RRRRR };
