import { createAction, ActionsUnion } from "../../shared";
import { Recipe } from "..";

export const ActionTypes = {
  FETCH_RECEPTS_PENDING: "FETCH_RECEPTS_PENDING" as "FETCH_RECEPTS_PENDING",
  FETCH_RECIPES_SUCCESSFULLY: "FETCH_RECIPES_SUCCESSFULLY" as "FETCH_RECIPES_SUCCESSFULLY",
  FETCH_RECIPES_REJECTED: "FETCH_RECIPES_REJECTED" as "FETCH_RECIPES_REJECTED"
};

export const Actions = {
  loadRecipes: () => createAction(ActionTypes.FETCH_RECEPTS_PENDING),
  loadRecipesSuccessfully: (recipes: Recipe[]) =>
    createAction(ActionTypes.FETCH_RECIPES_SUCCESSFULLY, { recipes }),
  loadRecipesRejected: (error: string) => createAction(ActionTypes.FETCH_RECIPES_REJECTED)
};

export type Actions = ActionsUnion<typeof Actions>;
