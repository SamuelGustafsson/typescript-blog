interface Recipe {
  _id?: string;
  title: string;
  description: string;
  servings: number;
  ingredients: Array<string>;
  instructions: Array<string>;
  imageUrl: string;
  cookingTime: number;
}

export { Recipe };
