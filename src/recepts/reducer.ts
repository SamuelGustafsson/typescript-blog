import { Actions, ActionTypes } from "../actions";
import { Recipe } from "src/recepts";

interface State {
  loading: boolean;
  recepts: Array<Recipe>;
  favorites?: any;
  selectedRecipe?: Recipe;
}

const initialState: State = {
  loading: false,
  recepts: []
};

const reducer = (state = initialState, action: Actions): State => {
  switch (action.type) {
    case ActionTypes.FETCH_RECEPTS_PENDING:
      return { ...state, loading: true };

    case ActionTypes.FETCH_RECIPES_SUCCESSFULLY:
      return { ...state, loading: false, recepts: action.payload.recipes };

    case ActionTypes.SELECT_RECIPE:
      return { ...state, selectedRecipe: action.payload.recipe };

    case ActionTypes.FETCH_FAVORITE_RECIPES_SUCCESSFULLY:
      return { ...state, loading: false, favorites: action.payload.recipes };

    default:
      return state;
  }
};

export { reducer as receptReducer };
