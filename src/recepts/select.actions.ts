import { createAction, ActionsUnion } from "src/shared";
import { Recipe } from ".";

export const ActionTypes = {
  SELECT_RECIPE: "SELECT_RECIPE" as "SELECT_RECIPE"
};

export const Actions = {
  selectedRecipe: (recipe: Recipe) => createAction(ActionTypes.SELECT_RECIPE, { recipe })
};

export type Actions = ActionsUnion<typeof Actions>;
