import * as React from "react";
import { styled } from "../../shared";

type InjectedProps = {
  content: string;
};

const Description = (props: InjectedProps) => <Content>{props.content}</Content>;

const Content = styled.p`
  font-family: ${props => props.theme.fonts.primary};
  font-size: 16px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.25;
  letter-spacing: normal;
  text-align: left;
  color: ${props => props.theme.colors.primary.color};
  padding: 0 ${props => props.theme.mobile.padding}px;
`;

export { Description };
