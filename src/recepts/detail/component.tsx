import * as React from "react";
import { AppState } from "src/store";
import { connect } from "react-redux";
import { RecipeDetailPage } from "./recipeComponent";
import { Recipe } from "src/recepts";
import { history } from "src/history";

type Props = InjectedStateProps;

type InjectedStateProps = {
  selectedRecipe: Recipe;
};

class RecipeComponent extends React.Component<Props> {
  constructor(props: Props) {
    super(props);

    !this.props.selectedRecipe && history.push("/");
  }

  render() {
    return (
      <>
        {this.props.selectedRecipe ? (
          <RecipeDetailPage recipe={this.props.selectedRecipe} />
        ) : (
          <div>DET FINNS INGE VALD PRODUKT</div>
        )}
      </>
    );
  }
}

const mapStateToProps = (state: AppState): InjectedStateProps => ({
  selectedRecipe: state.recept.selectedRecipe!
});

const DetailPage = connect(mapStateToProps)(RecipeComponent);

export { DetailPage };
