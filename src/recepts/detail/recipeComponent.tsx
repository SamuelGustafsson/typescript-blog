import * as React from "react";
import { Description } from "./description";
import { styled, theme, firebase } from "src/shared";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Recipe } from "../recipe";
import { Dispatch } from "redux";
import { Actions } from "src/actions";
import { connect } from "react-redux";
import { AppState } from "src/store";

type Props = InjectedProps & InjectedDispatchProps & InjectedStateProps;

type InjectedProps = {
  recipe: Recipe;
};

type InjectedStateProps = {
  user?: firebase.User;
};

type InjectedDispatchProps = {
  addToFavorites: (recipe: Recipe) => void;
};

type ComponentState = {
  isAdded: boolean;
};

const addFavorite = (userId: string, recipe: Recipe) => {
  firebase
    .database()
    .ref(`users/${userId}/favorites`)
    .push(recipe);
};

class RecipeDetailPageComponent extends React.Component<Props, ComponentState> {
  state = {
    isAdded: false
  };

  toggleAdded = () => {
    this.setState({ isAdded: true });
  };

  render() {
    const { imageUrl, cookingTime, title, description } = this.props.recipe;

    return (
      <section>
        <Img src={imageUrl} alt="" />
        <Row>
          <CookingTime>{cookingTime.toString()} min</CookingTime>
          <FavoriteButton
            onClick={() => {
              this.toggleAdded();
              !this.state.isAdded && addFavorite(this.props.user!.uid, this.props.recipe);
            }}
          >
            <Heart
              icon={faHeart}
              color={!this.state.isAdded ? theme.colors.secondary.color : "red"}
              size="2x"
            />
          </FavoriteButton>
        </Row>
        <Title>{title}</Title>
        <Description content={description} />
      </section>
    );
  }
}
const Title = styled.h1`
  font-family: Roboto;
  font-size: 32px;
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  line-height: 0.63;
  letter-spacing: normal;
  text-align: left;
  color: ${props => props.theme.colors.primary.color};
  padding: 0 ${props => props.theme.mobile.padding}px;
`;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 ${props => props.theme.mobile.padding}px;
  height: 90px;
`;

const FavoriteButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 48.9px;
  height: 48.9px;
  background-color: ${props => props.theme.colors.background.grey};
  box-shadow: 0 6px 6px 0 rgba(0, 0, 0, 0.24);
  border-radius: 80%;
`;

const Heart = styled(FontAwesomeIcon)`
  width: 33.5px;
  height: 29.3px;
`;

const Img = styled.img`
  width: 100%;
  min-width: 320px;
  height: 204px;
`;

const CookingTime = styled.span`
  width: 65px;
  height: 24px;
  mix-blend-mode: undefined;
  font-family: Roboto;
  font-size: 20px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1;
  letter-spacing: normal;
  text-align: left;
  color: ${props => props.theme.colors.primary.dark};
`;
const mapDispatchToProps = (dispatch: Dispatch<Actions>): InjectedDispatchProps => ({
  addToFavorites: (recipe: Recipe) => dispatch(Actions.addRecipeToFavorites(recipe))
});

const mapStateToProps = (state: AppState): InjectedStateProps => ({
  user: state.authentication.user && state.authentication.user
});

const RecipeDetailPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(RecipeDetailPageComponent);

export { RecipeDetailPage };
