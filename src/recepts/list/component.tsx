import * as React from "react";
import { styled } from "../../shared";
import { Listitem } from ".";
import { connect } from "react-redux";
import { AppState } from "../../store";
import * as _ from "lodash";
import { Recipe } from "../recipe";
import { Spinner } from "../../shared/spinner";

type Props = InjectedStateProps;

type InjectedStateProps = {
  recipes: Array<Recipe>;
  loading: boolean;
};

const ListComponent = (props: Props) => {
  let recipeData = _.map(props.recipes, (recipe: Recipe) => {
    return <Listitem recipe={recipe} key={recipe._id} />;
  });

  return (
    <>
      {props.loading ? (
        <SpinnerContainer>
          <Spinner />
        </SpinnerContainer>
      ) : (
        <Container>{recipeData}</Container>
      )}
    </>
  );
};

const SpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

const Container = styled.div`
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;
`;

const mapStateToProps = (state: AppState): InjectedStateProps => ({
  recipes: state.recept.recepts && state.recept.recepts,
  loading: state.recept.loading
});

const RecipeList = connect(mapStateToProps)(ListComponent);
export { RecipeList };
