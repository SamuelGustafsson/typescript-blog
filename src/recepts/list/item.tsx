import * as React from "react";
import { styled, VerticalDivider } from "src/shared";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { Recipe } from "../recipe";
import { Dispatch } from "redux";
import { Actions } from "src/actions";

type Props = InjectedProps & InjectedDispatchProps;

type InjectedProps = {
  recipe: Recipe;
};

type InjectedDispatchProps = {
  selectRecipe: (recipe: Recipe) => void;
};

const ListitemComponent = (props: Props) => {
  const { imageUrl, title } = props.recipe;
  return (
    <div>
      <VerticalDivider height={30} />
      <NavLink onClick={() => props.selectRecipe(props.recipe)} to="/recipe">
        <Container>
          <Img src={imageUrl} alt="" />
          <Div>
            <p>{title}</p>
          </Div>
        </Container>
      </NavLink>
    </div>
  );
};

const Img = styled.img`
  width: 157px;
  height: 139px;
`;

const Div = styled.div`
  display: flex;
  justify-items: center;
  align-items: center;
  width: 157px;
  height: 50px;
`;
const Container = styled.div`
  display: flex;
  flex-direction: column;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);

  &:hover {
    cursor: pointer;
  }
`;

const mapDispatchToProps = (dispatch: Dispatch<Actions>): InjectedDispatchProps => ({
  selectRecipe: (recipe: Recipe) => dispatch(Actions.selectedRecipe(recipe))
});

const Listitem = connect(
  null,
  mapDispatchToProps
)(ListitemComponent);

export { Listitem };
