import { createAction, ActionsUnion } from "../../shared";
import { Recipe } from "..";

export const ActionTypes = {
  FETCH_FAVORITE_RECIPES_PENDING: "FETCH_FAVORITE_RECIPES_PENDING" as "FETCH_FAVORITE_RECIPES_PENDING",
  FETCH_FAVORITE_RECIPES_SUCCESSFULLY: "FETCH_FAVORITE_RECIPES_SUCCESSFULLY" as "FETCH_FAVORITE_RECIPES_SUCCESSFULLY",
  FETCH_FAVORITE_RECIPES_REJECTED: "FETCH_FAVORITE_RECIPES_REJECTED" as "FETCH_FAVORITE_RECIPES_REJECTED"
};

export const Actions = {
  loadFavoriteRecipes: () => createAction(ActionTypes.FETCH_FAVORITE_RECIPES_PENDING),
  loadFavoriteRecipesSuccessfully: (recipes: Recipe[]) =>
    createAction(ActionTypes.FETCH_FAVORITE_RECIPES_SUCCESSFULLY, { recipes }),
  loadFavoriteRecipesRejected: (error: string) =>
    createAction(ActionTypes.FETCH_FAVORITE_RECIPES_REJECTED, { error })
};

export type Actions = ActionsUnion<typeof Actions>;
