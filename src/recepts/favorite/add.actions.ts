import { createAction, ActionsUnion } from "../../shared";
import { Recipe } from "..";

export const ActionTypes = {
  ADD_RECIPE_TO_FAVORITES: "ADD_RECIPE_TO_FAVORITES" as "ADD_RECIPE_TO_FAVORITES",
  REMOVE_RECIPE_FROM_FAVORITES: "REMOVE_RECIPE_FROM_FAVORITES" as "REMOVE_RECIPE_FROM_FAVORITES"
};

export const Actions = {
  addRecipeToFavorites: (recipe: Recipe) =>
    createAction(ActionTypes.ADD_RECIPE_TO_FAVORITES, { recipe }),
  removeRecipeToFavorites: (recipe: Recipe) =>
    createAction(ActionTypes.REMOVE_RECIPE_FROM_FAVORITES, { recipe })
};

export type Actions = ActionsUnion<typeof Actions>;
