import * as React from "react";
import { Recipe } from "../recipe";
import { styled, Button } from "src/shared";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes as faExit } from "@fortawesome/free-solid-svg-icons";

type Props = InjectedProps & Dispatchprops;

type InjectedProps = {
  recipe: Recipe;
  dataKey: string;
};

type Dispatchprops = {
  remove: (recipe: Recipe, key: string) => void;
  //   remove: (key: string) => void;
};

const Item = (props: Props) => (
  <Container>
    <p>{props.recipe.title}</p>
    <Button
      onClick={() => {
        props.remove(props.recipe, props.dataKey);
      }}
    >
      <FontAwesomeIcon icon={faExit} size="lg" />
    </Button>
  </Container>
);

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 24px 0 24px;
`;

export { Item };
