import { ActionsUnion } from "src/shared";
import * as load from "./load.actions";
import * as add from "./add.actions";

export const ActionTypes = {
  ...load.ActionTypes,
  ...add.ActionTypes
};
export const Actions = {
  ...load.Actions,
  ...add.Actions
};

export type Actions = ActionsUnion<typeof Actions>;
