import * as React from "react";
import { Recipe } from "../recipe";
import * as _ from "lodash";
import { Item } from "./item";
import { VerticalDivider, firebase, Text, styled } from "src/shared";
import { connect } from "react-redux";
import { AppState } from "src/store";

type Props = InjectedStateProps;

type InjectedStateProps = {
  authenticated: boolean;
  recipes?: Array<Recipe>;
  user?: firebase.User;
};

class FavoritesListComponent extends React.Component<Props> {
  removeRecipe = (recipe: Recipe, key: string) => {
    firebase
      .database()
      .ref(`/users/${this.props.user!.uid}/favorites/${key}`)
      .remove();
  };

  render() {
    return (
      <Container>
        <VerticalDivider height={30} />

        {this.props.authenticated && this.props.user && this.props.recipes ? (
          _.map(this.props.recipes, (recipe, index) => {
            return (
              <Item
                recipe={recipe}
                key={index}
                remove={this.removeRecipe}
                dataKey={index.toString()}
              />
            );
          })
        ) : (
          <EmptyText value="Du har inte lagt till några favoriter ännu!" />
        )}
      </Container>
    );
  }
}

const Container = styled.section`
  padding-left: 24px;
`;
const EmptyText = styled(Text)`
  padding-left: 24px;
`;

const mapStateToProps = (state: AppState): InjectedStateProps => ({
  authenticated: state.authentication.authenticated,
  user: state.authentication.user && state.authentication.user,
  recipes: state.recept.favorites && state.recept.favorites
});

const Favorites = connect(mapStateToProps)(FavoritesListComponent);

export { Favorites };
