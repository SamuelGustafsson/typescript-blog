import { ActionsUnion } from "../shared";
import * as load from "./load/actions";
import * as select from "./select.actions";
import * as favorite from "./favorite/actions";

export const ActionTypes = {
  ...load.ActionTypes,
  ...select.ActionTypes,
  ...favorite.ActionTypes
};
export const Actions = {
  ...load.Actions,
  ...select.Actions,
  ...favorite.Actions
};

export type Actions = ActionsUnion<typeof Actions>;
