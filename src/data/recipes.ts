import { Recipe } from "../recepts";

const peaSoup: Recipe = {
  cookingTime: 46,
  title: "soppa på gröna ärtor",
  description:
    "Denna grönärtsoppa är barnsligt god. De milda ärtorna tillsammans med bönor och grädde gör denna soppa magiskt krämig. Soppan serveras med nygräddade baguetter med en röra gjord på färskost och räkor. Toppa även soppan med räkor för extra smaksensation.",
  servings: 4,
  ingredients: [
    "1 gul lök",
    "1 vitlöksklyfta",
    "380 g kokta vita bönor",
    "1 msk olja",
    "2 dl vispgrädde",
    "7 1/2 dl grönsaksbuljong (vatten och tärning eller fond)",
    "600 g gröna ärter",
    "salt",
    "peppar"
  ],
  instructions: [
    "Tina räkorna och låt dem rinna av ordentligt.",
    "Sätt på ugnen enligt anvisningen på brödförpackningen.",
    "Skala och hacka lök och vitlök. Spola bönorna i kallt vatten och låt rinna av. Fräs lök och vitlök i oljan i en kastrull tills löken mjuknat. Tillsätt bönor, grädde och buljong och låt koka några minuter.",
    "Häll i hälften av ärtorna och låt sjuda ca 5 minuter. Mixa soppan slät med en stavmixer eller i en blender och smaka av med salt och peppar.",
    "Koka upp soppan med resten av ärtorna och låt sjuda några minuter tills de tinat.",
    "Servera soppan med resten av räkorna, baguetter och räkröra."
  ],
  imageUrl:
    "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSEhMVFRUVFxcYFhgYGBUVFxUVFRUWFhUXFRYYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy8lICUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAKQBMwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAEAAIDBQYBBwj/xAA8EAABAwMCBAQDBgUEAQUAAAABAAIDBBEhBTEGEkFRImFxgRORoQcUMkKxwSNSctHwFjNi4ZJDU4Ki8f/EABoBAAMBAQEBAAAAAAAAAAAAAAECAwQABQb/xAAnEQACAgICAgICAwADAAAAAAAAAQIRAyESMRNBBFEikRQyYXGhsf/aAAwDAQACEQMRAD8A1T23QlfII2klXtdyQNLnEXXl3E+vmR3KzJ6ALzJa17N/L2AcQ6rzEgZPRVem6M+Z1yCtLw7wk+Uh8g3XpWkcPMjAwpvJWo/szznZktA4OAsXBbWh0drALAK2igAUpsEnD2ybZDFTgJtVVNjFyQgNX1tkTSSV5fxLxiX3AOF13qJxpuJeMg27WFeZaxr7pCS511TV2pl53UMdOXZOVoh8etyOGT1bnbJsMZPmVcQ6Rcc0h+G3z3PoF11eyL/aaAe+5K0f4kVjib70PpNOLQHP8I89/kjjqscew5j5rPy1T5DdziuMASvHe2UUYR6X7Luo4jleCBZo8gqySpcTe5Pqhw2+yljbhMopdDOTY1rjt1XWjPmu8tjdSNjxfr0XNgOcubKVjL7mx2TGMIHmig0ABI5FIpEDR9FNa1jgp72NtjddZHjAyUHIdKiSGP2RMDbeiHjj6fNEtbn9krZRMniAv5I6No3va/ZDUtt72z9VbRm+CBY9kLBKaTo5G8Wt9UZTNJH1QTje/YYRdP0TRaBNFrSHZWVNCq6kcBb9lZxEdFVPZnkmER3bt/0oK3S6eoFpGC5/MMFSfF7rsTldb7Eow3EH2duZ/EpyXWzb/pV2rao6OCMfnbhw7H0XrML0BrXD0FWPG0Nf0cO/mo5PjKW0TlGMu9HnDNTfLA1lyCLlx8lldSl6LWcRaZNRtMbm3aTh42I7HsVh6p1ysixtT2jPkhxYTpcx5uXurCspr3af8KqNMP8AEb6rRVTbm4TzjsRGUIe3HZJXslICSUl3kH0aHX+IJqqTkZck9tgtJwhwTa0kuXHurzhXg5lO0Ei7upPdbOGABQ2+i052C0lA1owEXyWU1lW6jqDYwSSjqKEJaicNFysbxJxY2MENOVRcV8Y7hpsvLNX15zybG6EMcsr10Mi84g4nc8m7lkKiqfIcXUQ5nHK0ek6OA34k7uRlrgW8T/6fLzW2OOONBjFydIq9N0yR5xgDcnAHur2GujgA5G88nVzth/SOiG1LVPiWY0Bkbfwgfv5quCanLsukodbYRU1Tnm7jdDPAKkcfJNcPJMtHXY1qext0rBLqg2CybHZPsCoQegR9JpkjzgWHmlYHJIFt0T2AnAC01Dwzf8RJWhoeH2N2aFKWWKE8tMw9Jp73H8BKsqbh6V2HAAfVb+DTAOiMZTAdFF/IQrzSvRjqXha9uZ3yCv6HhOC1zv0urpkQCnZGgs7T6EeST9mbfw3Fc2ak3hmPstMIFx7UjyvsPkn9mfHDbLWXHcNjoSFoCLJzZfdcsjBzl9lAeHfD+LJ/zK5/pqUW5SCPqtK146hSB3Y2VI5F7RSOea9mYk05zL+Fwt8ipWycrbX2ytP8Y9QCoH08TvxNsVaMoPpjL5D9meE4IRFLIrJ2gsP4SR9UGKJ8RyLjurxTRbnCXQax3lZTsKCY7Nj81PFL0V1IRxCaumZMwxytDmleP8fcGupn/EhBMZ2tmy9d+NYj/MLszGTNdE/IcP8ACEsoqQKVU+j5wg8JDuxWtYWEA3Fjj3TeLuHPu0xAFmk4/uqqnpysjM2XHwdMPlgsSLpKAtJ6lJT4smfR7GWTy6yhlmDd1l+IeKWRA2OVJySKltrOtMiaSSvIuL+M73AOFR8VcYOkJAN1iZHukNyqY8Dm7n0EmrtQfKSSTZMp6YlHUGludm2O601DSR00f3iQX/8AbafzH+Y/8QtlpaiGK5OgWj0tlMwTVDbk5jjPXs53l5Ku1LUHzO5nu9B0A6BR6hXvneXvcST9B2CHaur2aNJUh1tl1jj0SuLpwFlzYp1vzSJXLoqjoHPN+iArYJy36K1odFkec4CvtN0TbC0lLp9t0k5qKIyyfRRafoDW2x7rQ0emgbBWUFLjZWNNThefkyOTE2+wGCit0RjGWRkTw05bfsopbk7pGATLKZrLhQtLQCA3PdcpOYXzugnsJIxuUWxChlsrhdfqm2cWEbR8729UG2myXE9U+AlqWbnzTUnRx1sJOwuE51ORi1rqWkdynKMkka4WvZUjhuN+zrKSTwG1/dOLut1Nq1DzEcuyhjZyixUWnFtM4limUgcgYwQp2ldyYQl0hB8OETHMHYchGOT37K2PLKO0ziOt00jxM+X9lUxvO17EfRXtNUWxuFBqFAJPFHbm6ja62wyKatdl4ZN0ysbL5pwmO43UDRkjN27jt6hOaqRmauJHxXRCppi4C72DPey8lp3lpLT0NivZqc2uOjhY+68k4npvhVBA/Nf6JMi9kc0Lx/8AA4SNXVWCVJSswWekcUcZhoIaV5LrnET5nGxNlW1lbJO4knCsdE4fkmIs0272wjjwKG5dl2yrpqN0h2JJWooOHeQc8x5R26q9ZHDRtDW2kmOA0ZN1c6VoXxSJas5OzOgTSlJ9CcrKjT6JrwXuHLAzNti/sFk9f1N1RIXbNGGjoGjYLVfaDqYby08WGjssHdHGvZrUeCr2dFk9o7ppG1k9rU7YLHddkrnpuub4Cv8ARtLJyRlBulbElKkD6VpReeZw9lsdN0wCym06hA6K+pKP2WSfyPozuTZFTUtkfDTjqrBkbYoy9wvjw+Z7rPyVMrncwssmSbumci/jY21gM900v5TuhaaQkZRUcHMe6G29HDib+a4zsphZpt1UM8jRscphbHhiIY2yFpj13RscRcQNl26DohnCZFHZV/Ec/wAO4a83CyVJxrJHJyv8bOt9x6FTjkuTVE/Iro9EaFI4BuTlVtHqbJQC11wdrIivl5gGsxjJ81ZuSja7K6HGrJPhGFLDIDuo6KmDW279eye9oHqjHI1/Y6g6OVoBFgb9VX1kJwe6ZDIbqeYldOXNWcBArsZymuauxBSOCWqfyUUYU7HWVEqCT/d7NvZDvlFwFPNVFzbIP7vf1TSk7XE7R3UYAWFzRc3ALupCqg2ytxduL+oQNXEA4i/p6LbjmpKzTgn6IGLC8YRsbUxOkF2fEaHD/i42P6rc9VgvtAsZGDNzI0fNwVG7RoW7LN/C9Je4Jscj3yuprnMb4STcJKFoj419FbwhwG2wknabDNirzWNWZikoIw6Q4JAw3zKrazXqmveY6FpbGMOdsPZaPh/SINPjMkj7SEXcXbkpHb2zIAaFwKaf+PM4OkOSTsPRI62JHS8tuSnbcu6GQ3DQPqUDqvEstaSLmOnbu7YyeQ8kJxE9tPpwDGBnxnE268owD9E8ZNvZTFC5nnuoVBkkc8m5JKHaE6x/ddtj1V+kXbOtTulk5rLZKsNJpC9+RhButiSlQRoml38RGVtdPorDZR6bSAAYV1TxkLHly3ozN2TUVPm1lo6ahZym7s+ox8lTwNRb3covfJUYNLbQBmoy3HLuBshaSA9RdE09MXuzm6mrXCPwjLvJI7/szjscPX6IuAG/hICHpHkjIspS1Ovs4ZWVIabOsb4x/dZbUQ98nLGTvv2Wq+COynhoGBpdYXS5I8ttkpxYJptOQ0AonVYnsY17L2ze37291j9Z1WSJ3NG4ix9j6hX/AA/xW2dgsQH7Oaeh7+iGKcM6aegck1RjeI6l4uSHC/cHb3WDqZSXL1/jPUC1m4N74sFjtCgiqHkFo5v1Rw44wk4k4pJj+DHSAWzbot5C+wuQSmafpTYx0A+SJ1qr5IgQ3FvJdl5RKubRANdiB5SbE90ax4d1Xluq1D5XeEHf6rb8Nc/w28+4AS425R/IbHJy7NFDHlWH3S4ucDzQcTkXPV8zbdVogo1soZ3UKgMfyp9PNdcqqMuddS08HKoRUrCwuHzUnKuMCeWlX46AcCeHWTmsXZRZK00EheLm5Q1U0k3O2wPbyRMR6lM1LLQPNVwurKY3UipIsV55rchmroIwf/U5/Zvi/ZbnUpvhxlx9B7rzzRPHVyzXuGN5W+rt/oPqrqWjel+NmompA5xJ5bn0SQD5bm911SsWmWEOow6ZAG/mtsOpWa16eoqC2ers2I5YwHcdLoeiqh95+PWZNiY2HI8sIbVNVfUOaHAWbfkaNm+qd1ejzkiGv1IvHKRysb+Bg3PZaL7SrtgpGnpCy/qQCVS09HnDTJMRgDNlovtUjPJTOyP4TfawCaDW0i+F7Z5uDfClYy5suhilaqNjsTxewC2GhUIa0LO6TBzSC/TK3mnQ7LPmnSoz5GHUtPZWkUITKaLA79lZCheBzEWA/wAwsjTfRMjayyY93Nv028khX8t7b7dNlPpdM1/MXb2xlCMeTpBFE8giycIbm565KYzBIPRFxpgUOaxJx6J73WT6WMcwJQbvo66QG+sYwgPNkLrOomOIkZY7ZwyPfsoeLalouB+K3yXmGparICWhxsendZ7cm4f9ozym26J9X1LmJygdGlcH3aSFNHpD3gOLXZ8itFoGicuVpxYVCOtnRS9jZaOeo3KveFuEfgu53O8R+gV9FQBjAbgG3yVHqPEjoXZs4AEAjBHy3U/JGLqtgaUSXiaoDLgG9lhmcQyNkA5iWE2LTkWReovmqMxNLwe379kBRcNS84MtmC+xISYoVO5OhYv8rZvdMhjeA4NGfJX9PTAbCyB0ii5GgK5EIIJJs0b+q2y1s0J0gYzsbu8BSMla7III7g3WZ1+sbkNIWFh1+SGXwE53HQ+yyYczk2mgLJbPX7BCk52QWk1pe0HyR78qsioRG5TtyhI3eSmY8jZXhI4JUDgepTnPKblDI0zkOjaBuha0ZA9fqiwOndZzirVmwMcebxEYXY9FcMXKWjIcf66GAsbnp7ncqq0Sn+FCAd3+I/8Ay2HysqWJzquqa0m7S657WGStBWEkPb+UWsqS1o9FK3rpEsktja6Si+ATnlvt+i6lDoFqIIZZG8h5QAA997372UeuanBBG6KFrXG+JOp9VT69rrXWbEA0Nxjqs85rnm5Vo43JbPMSbPa+BoIoqf4xs57hck/oFzjqL7zQMlbksLmn2KwWicQOjj+G69hsVteAdXjqRPQvP+4Odl+9rOtf2Kz4VkjkafQuBSjk30eZxBTtYUbqWnGCZ8TuhNvmmCNaGzTJUWXDUV7nG437dVt6FnZZHhduSCey2tM1Zc3Zml2GRhWD6wujDLZ7+SCYp2Ef53UlNq0hQcU+bo6EADzQzXGxAtnqiYW2Fk0AE0cSNgfy9M/RR6efGLmyI1CBrRgm5T0+PJBBZncx2t/myKrIOZgdGbOAtbvZQUkZOQCVPLC4G1soJJxfJdgas881z40knI1juY4sArfRuHIaVnxZQJJz3yGf0jv5rQxTiN+bZKG16Nx8UbS8eVsH0Wd4lHG3DbIePjsyPE2pcoOd1U8McVFknJJ4mG/qPQpavolTM4nlLR3cUJo3D/w5QJHtv6ofHj49ydMRdm0qtRmmYAGFgAJ5jgEdLFZmHTn1EgjF7k5N74XolDF/DDbkgC1v2SoYmslBa3PUeXZV8UOabZTxexulaZDTR2a0AHHmbdSszxrOxtwywxmy2GsUtweUuab3sbYXnPFFDK873U88HKXGgSi6o7whxUQfgyZGeU9R5ei2Wr1xlYPhnlNsjofP1Xluk6HL8QOtsvRdMgIABWm04cGOo2qMrXaPO/qM+qZpHCTufmkznC9CjgCIEQGUIY4wWjo40gCio/htsEVYqXC7ypXsrR2JqmaxRMCID1WFHMkkbYWQgbZEE3QOs6nHTsMkh2GB1J8ks/yloaMW9Ibquptp4nPdvazR3K8Q4o1ySaR3iuDjy8lPxfxS+occkDoB0WZpmlzh3JWmEKWzZBeNUu2a3hClDQZDvsPQ7q2lI5ni2LfNBxfwiGgbNAHmRlEyPPM1xAyM+hUpbZqiqVFvBqjuUcpaBYWFklRsrg0Wvtf9UkbZLxosuFuAII6Zs9TZ0jhex2F9hZA6xp8BuGsDSOy0Vbq3NG1vQAfosvWVG5XmSzSyZE0zwZTlyDdL0BjowbXHdWNFoDY5GyR+F7Tdp7FA8F6hbnY51s3AK1weD037L0ot1Z7mOXKKbK7jXRvvcQqY22kZ/uN6gj/PksBDHcdiPoV6sS6ECZt7bOadnt7evZUOvcPNlH3qjyDl8fW/XHRV5LInXa7J3F6TMpoXhlseu3qtxSrEBmeYYLTkbEW3uthpFTztBWbJszZI0y4iYpQBY3OemN1HEFKG91EmdJFsX9+ykjuuNsntPVMmAkD7Ih9QXAA9EGG3UzEYthLmhkbY2sO/RMlddznBw2x5IKB4ByLo2JjOVzrf9LVF80v8AUlVBfxE5v2+qnoiQN1x8nNfGenop9PIAcH9Rb2WbEkp6OkAcQ1jWRkuI5j9LLyDV9VPPcHqvQuKdCkkPgkuPMbLL0/BZLruJKR3Kbc/Rm4Nytm0+z6rfNFkkYHz6LXNpQwFxO2T3KoOFaQQDlFgtFMOYEDrvfPyWqEYuF+zSimqJy59hsgtQpw7CLfHmzQfdExU4G+/nsFjUGxipbQCNgJsL7LlLUNJ8vJO1Z1hYlVGnxWONkOT5V6OovHznn8I8KOY/GUJTsRbGKqsAwtuVIwLr2qWOK6FV0EY5wCgjeSVFX1bIvE9wa0d1g+JPtBa27IMf8up9AjBSm9FIYnI2HEPE8dNGdi8dB0/qXjPE3EslS8lzyR9LdgqzVNWfMbuPt/dVpK3Y8dbZpSjBVH9jnOvurTRYr8zz0Fm+vUqrY3mIA//AAK+h8J5Rhthbuml9HR7LN9UcOG4ISnrgWizgSCMX81QVdUOTlBPNf2sP1TaYC3ndRkqJz+VxdJF7LGSSRsUkDHWOAtfZJLyH/kxN/WaO74Qc38VlS6Nw3VVj3CwjY3dxzfyC9J+73FiE19V8CNwAt5qORQh+TRD5UYxjyrZgazhJlM7m+M8u87WK0mkOaWgg7LMcQak5xNyiOBqn4gmjd2x5EqGOc5p716IfFzSdw/RoOJtYuAwbWWb0TX3wSkB1r7X/C4/yn9kLqVS9hLZGkFp36H3WfLZKiQRwtL3nYD/ADCX48Z8+TeyGJzhOz06ekpq8c8bhDON+zu4I6qlbDLSSWlaWg9Rlh8wVWO0mopw18vhIsHEG5A6Eq+puJ3AfCqWiWPbm3Nl6Hlhk1Lv7PTSU4l7SShwBGQjGKipaKNx56OcMvkxuy3/AKRn32WM2mhcB/OzxMP7hRnha6JSwtdFwxo72TpGcu+fRC01WyTLHA++fkigTZTT9Emq7HNSc49AnMF8YCi5yD6IsBLtupoqojAQlSS83Jt6JsMVvNFOSejgprLm6IJaBsb97/smw5RdZT/yjonUXTaOKd7TfOVxsCIDVwhS6Oo4xlhspIrprSp4GXNk1/RwTCADci6r62oAaTm/Tsj6thaLFUE4ud0mWTWglVXvdIfJGafHjZSsp0QSyMXc4BShGtndhcAspuZZ2s4sp4huXHy/uspq/wBoW/IQ0eXid/0tKjKXRWOGTPQ6mrjjy94b6lZbWftFZEC2K39R/YdV5Zq3EcsxJufUm5VM4k5JutEPj12WjjjHvf8A4Xmu8TTVDiS8+/7DoqFzjuuhMkK0JJaQzbY26ROEwlSxN2JREsLpGAA9yiBNgG+dkNG7KUhAvf1CVgbpALZvEbo2OddhyeiVTBsWi2c+iRtN0ZXUnR01C6uthFspI8YlfEfRbYnt81QcVxSW5o7lv5gOhHVaSkrWvF9wcgp1cwctwLg4Wb5MIOH5FM6jKNTPD9Uqt74PZbzgfQ46eA1E3+5IMDsOnuma7o9LfmLAHXBv1xlAV/EwsWA2HZeb5vx4wR5jcYaiLXq4OuDYgoj7NoYqcTSiznO27taOnzWN1HUgbm67pWtMYyzXHmN+Yd+1lXDiyxxtxDjhJptGm4g1SSVxsCSegyqekZIRYtc1w6EEXHkT+i9K4XoY4qUTPaOd45iTuAdgsjxDqhDzyGwukV40m9t/sOPLLE7+zNCZzXeElpHZW9FxRUMFi7mHnuq77/G53iFnd+h9f7qBzRcgHfZbITZ62PKsitGnh12mkdzSRkO7tJaforin1Nlrx1Nv+LxzbbZGV5xO0tKTHK6aa2irjy7PUINZktljH/0OHTycp262wZeyRnq0kfMXXlgqXDZxUrNcnbs8oeODJv48WerRavAdpW+hx+qKZUsOzmn3C8l/1LL+YNd7KL/UB/kb+i5Yl9k38ZemezRyDoVY09Z0d814QziJw/L/APYhSf6oI2a7/wA3JoxcemD+N/p7xVQhwuN1VSEA5Ix5rxl3Fcu3i/8AN391A/iiT+Ue5JQyY+TtHfxvtnsz6pg/O0e4T6fX6eM+OZgt53/ReEya/J2CDqNWlf8Amt6YQhip2d4I/Z7vqvHlIAbOc70ac+5WRruP4j+CMNN93H9l5ZNUPdu4n3UBTvCpbY/CCekbmu+0CU3aCB25R+5Wdq+JJ33u4nzJuqhNKpHHGPSGuutE01ZI/wDE4kKCyV1wlODs6kk1cc6y4B0lQPK656ia/KYWUq6Jo2eV0SI8bLsTFK52FJydmV5HeiNrSdghqt5wbHzTHTZNj1+qKoHPGXde6d62Ub1sjpqnFkUyW+FLVFjmn+EOboRj5ofT6Z26nSexYwTdhpYkpWwm2R9UkLNXJHr09a6KVrGWDSbWtiykrNSkALQcAm3zXUl89kyT8ff2eVOUnHsyWsVLnblUlDSMl+MHi/KwEHYg3SSWr4Qfiq8mzE1UhLi25tcoqlYCAkkval/U3RSPaZ6p33WMXxyt/QLz/WZjcpJLw8W57PKl/crdPlPx2e/6FbjVuHYWUfxm8wf+K9+/S1rWSSW/LpKjdBtRVfZkGvLhlPlYG7JJIo9aJG4qIlJJWQ4xwTLJJInEbgmFcSRAxpTSuJLiTGEqIpJJkINCakknFOtG6jSSROGgpFJJccPGygmKSS5dk2CE5XUklUUMpZDZPkedkklF9kq/InljAbsiqVgO6SSjkegZ+yWobbZL4xbtZJJIuicGGNmxsEkkkCx//9k="
};

const caesarsalad: Recipe = {
  cookingTime: 45,
  title: "Caesarsallad med krutonger",
  description:
    "I caesardressing används sardeller, vitlök och vinäger som ger den dess karaktäristiska smak.",
  servings: 4,
  ingredients: [
    "2 st romansallad, i mindre bitar",
    "en näve Zeta Krutonger Naturella Zeta Krutonger Naturella, (brödkrutonger)",
    "2 äggulor",
    "1 vitlöksklyfta, finhackad",
    "1,5-2 tsk Zeta Sardellcrème Zeta Sardellcrème eller 2 Zeta Sardeller Zeta Sardeller, finhackade",
    "1 dl Zeta Rapsolja",
    "1 dl Zeta Extra jungfruolivolja Classico",
    "50 g Zeta Parmigiano Reggiano 22 mån Zeta Parmigiano Reggiano 22 mån, riven",
    "3-4 msk mjölk (om man vill ha en mildare smak)",
    "4 msk Zeta Rödvinsvinäger",
    "salt",
    "svartpeppar",
    "Zeta Parmigiano Reggiano 22 mån Zeta Parmigiano Reggiano 22 mån, hyvlad"
  ],
  instructions: [
    "Vispa ihop äggulor, vitlök och sardeller till dressingen.",
    "Tillsätt oljan droppvis under vispning.",
    "Rör i övriga ingredienser och smaksätt med salt och peppar.",
    "Lägg i salladen i en skål.",
    "Vänd ner dressingen och krutongerna.",
    "Garnera med hyvlad parmensanost."
  ],
  imageUrl:
    "https://www.zeta.nu/wp-content/uploads/auto-cropped/2016/04/Caesarsallad_med_krutonger-1024x683.jpg"
};

const friedFish: Recipe = {
  cookingTime: 45,
  title: "Stekt sej med dillsås och ärtor",
  description:
    "Sej är en torskfisk med lite mörkare kött än torsken. Stek den med smör så får du ett fantastiskt fast och smakrikt kött. Servera med potatis och den goda såsen med crème fraiche. Vem sa att fisk behöver vara krångligt?",
  servings: 4,
  ingredients: [
    "600 g sejfilé",
    "900 g potatis",
    "200 g frusna sockerärtor",
    "2 dl lätt crème fraiche",
    "1 msk fryst dill",
    "2 msk smör",
    "salt",
    "peppar"
  ],
  instructions: [
    "Tina fisken om fryst används.",
    "Koka potatisen och koka sockerärtorna med potatisen ca 2 minuter.",
    "Blanda crème fraiche och dill och smaka av med salt och peppar.",
    "Stek fisken i smöret och krydda med salt och peppar."
  ],
  imageUrl:
    "https://www.ica.se//icase.azureedge.net/imagevaultfiles/id_179740/cf_259/stekt-sej-med-dillsas-och-artor-723639_v16_580x580.jpg"
};

const samonWithShrooms: Recipe = {
  cookingTime: 45,
  title: "Ugnsbakad lax med kantareller",
  description:
    "Få smaker gifter sig så väl som lax med kantareller! Bjud på ugnsbakad lax med kantareller, bönor och blomkål toppat med hasselnötter och pecorino eller parmesan. En lättlagad middag som sköter sig själv i ugnen!",
  servings: 4,
  ingredients: [
    "600 g sejfilé",
    "900 g potatis",
    "200 g frusna sockerärtor",
    "2 dl lätt crème fraiche",
    "1 msk fryst dill",
    "2 msk smör",
    "salt",
    "peppar"
  ],
  instructions: [
    "Tina fisken om fryst används.",
    "Koka potatisen och koka sockerärtorna med potatisen ca 2 minuter.",
    "Blanda crème fraiche och dill och smaka av med salt och peppar.",
    "Stek fisken i smöret och krydda med salt och peppar."
  ],
  imageUrl:
    "https://www.ica.se//icase.azureedge.net/imagevaultfiles/id_184629/cf_259/ugnsbakad-lax-med-kantareller-farska-bonor-och-has.jpg"
};
export { peaSoup, caesarsalad, friedFish, samonWithShrooms };
