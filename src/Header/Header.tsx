import * as React from "react";
import { styled, WindowResizeTracker, Brand } from "../shared";
import { AppState } from "src/store";
import { connect } from "react-redux";
import { Actions } from "../actions";
import { HamburgerMenu } from "./hamburgerMenu";
import { Dispatch } from "redux";
type Props = InjectedStateProps & InjectedDispatchProps;

type InjectedStateProps = {
  authenticated: boolean;
};

type InjectedDispatchProps = {
  logout: () => void;
};

const HeaderComponent = (props: Props) => {
  return (
    <Container>
      <WindowResizeTracker>
        {(width: number, height: number) => (
          <HamburgerMenu height={height} width={width} />
        )}
      </WindowResizeTracker>

      <Brand>Cook It</Brand>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: left;
  align-items: center;
  height: 50px;
  background-color: ${props => props.theme.colors.primary.color};
  padding: 0 ${props => props.theme.mobile.padding}px;
`;

const mapStateToProps = (state: AppState): InjectedStateProps => ({
  authenticated: state.authentication.authenticated
});

const mapDispatchToProps = (dispatch: Dispatch<Actions>): InjectedDispatchProps => ({
  logout: () => dispatch(Actions.logout())
});

const Header = connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderComponent);

export { Header };
