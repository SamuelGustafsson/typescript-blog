import * as React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

const HamburgerButton = () => <FontAwesomeIcon icon={faBars} color="white" size="lg" />;

export { HamburgerButton };
