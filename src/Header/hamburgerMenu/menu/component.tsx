import * as React from "react";
import { connect } from "react-redux";
import {
  withProps,
  styled,
  Text,
  VerticalDivider,
  HorizontalDivider,
  firebase
} from "src/shared";
import { AppState } from "src/store";
import { Header } from "./header";
import { Navigation } from "./navigation";
import { Footer } from "./footer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";

type Props = StateProps & DispatchProps & InjectedStateProps;

type StateProps = {
  length: number;
  show: boolean;
};
type DispatchProps = {
  hide: () => void;
};

type InjectedStateProps = {
  user?: firebase.User;
  authenticated: boolean;
};

const MenuComponent = (props: Props) => {
  const { length, hide, show, authenticated } = props;

  return (
    <Container width={length} show={show}>
      <Header hide={hide} username={"Samuel"} />
      <VerticalDivider height={30} />
      <IconDetail>
        <Icon>
          <FontAwesomeIcon icon={faUser} size="1x" />
        </Icon>
        <HorizontalDivider width={10} />
        <p>
          <HeaderText
            value={authenticated && props.user ? props.user.email! : "Ej inloggad"}
            caseType="upperCaseFirst"
            fontSize={16}
          />
        </p>
      </IconDetail>
      <Navigation hide={hide} />
      <Footer hide={hide} authenticated={authenticated} />
    </Container>
  );
};

const HeaderText = styled(Text)`
  font-size: 14px;
`;
const Icon = styled.div`
  display: flex;
  width: 16px;
  justify-content: center;
  align-items: center;
`;
const IconDetail = styled.div`
  display: flex;
  align-items: center;
  padding-left: 30px;
`;

const Container = withProps<{ width?: number; show?: boolean }>(styled.div)`
    position: fixed;
    top: 0;
    left: ${props => props.width}px;
    width: 100%;
    height: 100%;
    background-color: white;
    z-index: 1000;
    transition: left 0.3s ease-in-out, visibility 0.3s ease-in-out;
    visibility: ${props => (props.show ? "visible" : "collapse")}

    display: flex;
    flex-direction: column;
`;

export { Container };

const mapStateToProps = (state: AppState): InjectedStateProps => ({
  authenticated: state.authentication.authenticated,
  user: state.authentication.user && state.authentication.user
});

const Menu = connect(mapStateToProps)(MenuComponent);

export { Menu };
