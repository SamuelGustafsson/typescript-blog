import * as React from "react";
import { faTimes as faExit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Brand } from "../../../shared";

import { styled } from "../../../shared";

interface Props {
  hide: () => void;
  username?: string;
  selectedRetailer?: string;
}

const Header = (props: Props) => (
  <Container>
    <Brand>Cook It</Brand>
    <ExitButton onClick={props.hide}>
      <FontAwesomeIcon icon={faExit} size="2x" />
    </ExitButton>
  </Container>
);

const ExitButton = styled.a`
  display: block;
  cursor: pointer;
  margin-top: -5px;
`;

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  background-color: ${props => props.theme.colors.primary.dark};
  height: 50px;
  padding: 0 16px;
`;

export { Header };
