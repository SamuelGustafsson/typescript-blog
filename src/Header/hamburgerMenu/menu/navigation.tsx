import * as React from "react";
import { styled } from "../../../shared";

import { NavLink } from "react-router-dom";

const Navigation = (props: { hide: () => void }) => {
  return (
    <Nav>
      <ul>
        <li>
          <NavLink onClick={props.hide} to="/receipt/detail">
            Recepts
          </NavLink>
        </li>
        <li>
          <NavLink onClick={props.hide} to="/favorites">
            Favoriter
          </NavLink>
        </li>
      </ul>
    </Nav>
  );
};

const Nav = styled.nav`
  height: 100%;

  > ul {
    list-style-type: none;
    padding: 34px 0 0 24px;
    margin: 0;

    li {
      margin-top: 15px;

      a {
        font-size: 20px;
        text-decoration: none;
        color: inherit;
        cursor: pointer;
      }
    }
  }
`;

export { Navigation };
