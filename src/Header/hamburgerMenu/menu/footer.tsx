import * as React from "react";

import { Button, styled, Title } from "src/shared";
import { store } from "src";
import { Actions } from "src/actions";

interface Props {
  authenticated: boolean;
  hide: () => void;
}

const SignInOutButton = styled(Button)`
  display: flex;
  justify-content: center;
  background-color: black;
  height: 44px;

  font-size: 1rem;
  font-weight: bold;
`;

const Footer = (props: Props) => {
  const buttonText = props.authenticated ? "LOGGA UT" : "LOGGA IN";

  const handleClick = () => {
    props.hide();
    props.authenticated && store.dispatch(Actions.logout());
  };

  return (
    <>
      <SignInOutButton onClick={handleClick}>
        <Title value={buttonText} caseType="upperCase" />
      </SignInOutButton>
    </>
  );
};

export { Footer };
