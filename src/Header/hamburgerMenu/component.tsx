import * as React from "react";
import * as ReactDOM from "react-dom";
import { Menu } from ".";
import { HamburgerButton } from ".";
import { styled } from "../../shared";

type Props = {
  width: number;
  height: number;
};

type State = {
  show: boolean;
};

class HamburgerMenu extends React.Component<Props, State> {
  state = {
    show: false
  };

  toggle = () => this.setState({ show: !this.state.show });

  renderHamburgerMenu = () => {
    const length = this.state.show ? 0 : -this.props.width;

    return ReactDOM.createPortal(
      <Menu hide={this.toggle} show={this.state.show} length={length} />,
      document.getElementById("react-portal") as Element
    );
  };

  render() {
    return (
      <div>
        <MenuButton onClick={this.toggle}>
          <HamburgerButton />
        </MenuButton>
        {this.renderHamburgerMenu()}
      </div>
    );
  }
}

const MenuButton = styled.a`
  display: block;
  cursor: pointer;
`;

export { HamburgerMenu };
