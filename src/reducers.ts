import { combineReducers } from "redux";

import { authReducer as authentication } from "./authentication/reducer";
import { receptReducer as recept } from "./recepts/reducer";

export const rootReducer = combineReducers({
  authentication,
  recept
});
