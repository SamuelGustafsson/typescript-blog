import * as React from "react";
import * as ReactDOM from "react-dom";
import registerServiceWorker from "./registerServiceWorker";
import { baseStyles } from "./shared";
import { Provider } from "react-redux";
import { ConfigureStore } from "./store";
import { history } from "./history";
import { ConnectedRouter } from "connected-react-router";
import { ThemeProvider, theme } from "./shared";
import { Route } from "react-router";
import { App } from "src/app";

// Adding basestyles to project
baseStyles();

const store = ConfigureStore();

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ThemeProvider theme={theme}>
        <Route path="/" component={App} />
      </ThemeProvider>
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();

export { store };
